module prisonergo

go 1.15

require (
	github.com/hajimehoshi/ebiten/v2 v2.0.8
	github.com/ojrac/opensimplex-go v1.0.2
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb // indirect
)
