package main

import (
	"fmt"
	"image/color"
	"prisonergo/field"
	"prisonergo/rules"
	"sort"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

var (
	censusMap map[string]uint
)

type Game struct {
	rules  rules.Rules
	fields [][]field.Field
	round  int
	maxUID uint
}

func (g *Game) Update() error {
	//start := time.Now()

	for i := 0; i < iterations; i++ {
		g.round += 1

		g.rules.Work(g.fields)

		g.rules.Breed(g.fields, maxUID)
		maxUID += 1

		g.rules.Die(g.fields)

		censusMap = g.rules.Census(g.fields)
	}

	//fmt.Printf("Update: %s\n", time.Since(start))
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	//start := time.Now()

	for i := range g.fields {
		for k := range g.fields[i] {
			f := g.fields[i][k]
			var drawColor color.Color
			if !f.Liveable {
				drawColor = color.RGBA{0, 0, 255, 255}
			} else if f.HasPrisoner {
				drawColor = f.Prisoner.GetColor()
			} else {
				drawColor = color.White
			}
			ebitenutil.DrawRect(screen, float64(i)*pixelSize, float64(k)*pixelSize, float64(pixelSize), float64(pixelSize), drawColor)
		}
	}

	ebitenutil.DebugPrint(screen, fmt.Sprintf("Round %d", g.round))

	var printList sort.StringSlice
	for k, v := range censusMap {
		printList = append(printList, fmt.Sprintf("%s: %d", k, v))
	}
	sort.Sort(printList)

	printHeight := 10
	for _, s := range printList {
		ebitenutil.DebugPrintAt(screen, s, 0, printHeight)
		printHeight += 10
	}

	//fmt.Printf("Draw: %s\n", time.Since(start))
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return width * pixelSize, height * pixelSize
}
