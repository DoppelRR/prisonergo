package prisoners

import (
	"image/color"
	"math/rand"
)

type PrisonerInterface interface {
	GetUID() uint
	GetAge() uint
	IncrementAge()
	GetPoints() int
	AddPoints(int)

	GetColor() color.Color
	Coop(uint) bool
	Breed(uint) PrisonerInterface
	Result(uint, bool)
}

type Prisoner struct {
	Uid    uint
	Age    uint
	Points int
}

func (p *Prisoner) GetUID() uint {
	return p.Uid
}

func (p *Prisoner) GetAge() uint {
	return p.Age
}

func (p *Prisoner) IncrementAge() {
	p.Age += 1
}

func (p *Prisoner) GetPoints() int {
	return p.Points
}

func (p *Prisoner) AddPoints(poin int) {
	p.Points += poin
}

//Basic Prisoner implementations
//Random
type PrisonerRandom struct {
	*Prisoner
}

func (p *PrisonerRandom) GetColor() color.Color {
	return color.RGBA{uint8(rand.Intn(255)), uint8(rand.Intn(255)), uint8(rand.Intn(255)), 255}
}

func (p *PrisonerRandom) Coop(uid uint) bool {
	return rand.Float64() > 0.5
}

func (p *PrisonerRandom) Breed(uid uint) PrisonerInterface {
	return &PrisonerRandom{&Prisoner{uid, 0, 0}}
}

func (p *PrisonerRandom) Result(uid uint, res bool) {
	return
}

//Always Cooperate
type PrisonerCoop struct {
	*Prisoner
}

func (p *PrisonerCoop) GetColor() color.Color {
	return color.RGBA{0, 255, 0, 255}
}

func (p *PrisonerCoop) Coop(uid uint) bool {
	return true
}

func (p *PrisonerCoop) Breed(uid uint) PrisonerInterface {
	return &PrisonerCoop{&Prisoner{uid, 0, 0}}
}

func (p *PrisonerCoop) Result(uid uint, res bool) {
	return
}

//Always Betray
type PrisonerBetray struct {
	*Prisoner
}

func (p *PrisonerBetray) GetColor() color.Color {
	return color.RGBA{255, 0, 0, 255}
}

func (p *PrisonerBetray) Coop(uid uint) bool {
	return false
}

func (p *PrisonerBetray) Breed(uid uint) PrisonerInterface {
	return &PrisonerBetray{&Prisoner{uid, 0, 0}}
}

func (p *PrisonerBetray) Result(uid uint, res bool) {
	return
}

//Tit for Tat
type PrisonerTFT struct {
	*Prisoner
	Memory map[uint]bool //Remeber betrayal with true, because the default value is false, and Tit for Tat starts with the assumption, that everyone is nice
}

func (p *PrisonerTFT) GetColor() color.Color {
	return color.RGBA{255, 255, 0, 255}
}

func (p *PrisonerTFT) Coop(uid uint) bool {
	return !p.Memory[uid] //False => Other Prisoner Cooperated => return true; True => Other Prisoner betrayed => return false
}

func (p *PrisonerTFT) Breed(uid uint) PrisonerInterface {
	return &PrisonerTFT{&Prisoner{uid, 0, 0}, make(map[uint]bool)}
}

func (p *PrisonerTFT) Result(uid uint, res bool) {
	p.Memory[uid] = !res
	return
}

//Grudger
type PrisonerGrudger struct {
	*Prisoner
	Memory map[uint]bool //Remeber betrayal with true, because the default value is false, and Tit for Tat starts with the assumption, that everyone is nice
}

func (p *PrisonerGrudger) GetColor() color.Color {
	return color.RGBA{255, 0, 255, 255}
}

func (p *PrisonerGrudger) Coop(uid uint) bool {
	return !p.Memory[uid] //False => Other Prisoner Cooperated => return true; True => Other Prisoner betrayed => return false
}

func (p *PrisonerGrudger) Breed(uid uint) PrisonerInterface {
	return &PrisonerGrudger{&Prisoner{uid, 0, 0}, make(map[uint]bool)}
}

func (p *PrisonerGrudger) Result(uid uint, res bool) {
	if res == false {
		p.Memory[uid] = true
	}
	return
}

//Detective
type PrisonerDetective struct {
	*Prisoner
	Memory             map[uint]bool //Remeber betrayal with true, because the default value is false, and Tit for Tat starts with the assumption, that everyone is nice
	MemoryMeetingCount map[uint]uint
	MemoryEverCheated  map[uint]bool
}

func (p *PrisonerDetective) GetColor() color.Color {
	return color.RGBA{0, 0, 0, 255}
}

func (p *PrisonerDetective) Coop(uid uint) bool {
	p.MemoryMeetingCount[uid] += 1
	switch p.MemoryMeetingCount[uid] {
	case 1:
		return true
	case 2:
		return false
	case 3:
		return true
	case 4:
		return true
	default:
		everCheated := p.MemoryEverCheated[uid]
		lastCoop := !p.Memory[uid]
		return everCheated || lastCoop
	}
}

func (p *PrisonerDetective) Breed(uid uint) PrisonerInterface {
	return &PrisonerDetective{&Prisoner{uid, 0, 0}, make(map[uint]bool), make(map[uint]uint), make(map[uint]bool)}
}

func (p *PrisonerDetective) Result(uid uint, res bool) {
	if res == false {
		p.MemoryEverCheated[uid] = true
	}
	p.Memory[uid] = !res
	return
}
