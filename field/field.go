package field

import "prisonergo/prisoners"

type Field struct {
	HasPrisoner bool
	Liveable    bool
	Prisoner    prisoners.PrisonerInterface
	Width       int
	Height      int
}
