package main

import (
	"log"
	"math/rand"
	"prisonergo/field"
	"prisonergo/prisoners"
	"prisonergo/rules"
	"time"

	perlin "github.com/ojrac/opensimplex-go"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	width     = 60
	height    = 40
	pixelSize = 16

	startingPop = 1.0
	liveable    = 0.6

	iterations = 10
)

var (
	maxUID      uint
	totalFoe    float64
	totalFriend float64
	totalCount  int
)

func main() {
	rand.Seed(time.Now().UnixNano())
	perlinGen := perlin.New(time.Now().UnixNano())
	g := Game{
		rules: &rules.StandardRules{
			Width:            width,
			Height:           height,
			BreedScore:       80,
			PayoutTemptation: 2,
			PayoutReward:     1,
			PayoutPunishment: -1,
			PayoutSucker:     -2,
		},
	}

	g.fields = make([][]field.Field, width)
	for i := 0; i < width; i++ {
		g.fields[i] = make([]field.Field, height)
		for k := 0; k < height; k++ {
			noise := (perlinGen.Eval2(float64(i)*0.1, float64(k)*0.1) + 1) / 2
			lable := noise < liveable

			if !lable {
				g.fields[i][k] = field.Field{HasPrisoner: false, Liveable: false, Prisoner: nil, Width: i, Height: k}
			} else {
				if rand.Float64() < startingPop {
					switch rand.Intn(5) {
					case 0:
						g.fields[i][k] = field.Field{HasPrisoner: true, Liveable: true,
							Prisoner: &prisoners.PrisonerBetray{Prisoner: &prisoners.Prisoner{Uid: maxUID, Age: 0, Points: 0}}, Width: i, Height: k}
					case 1:
						g.fields[i][k] = field.Field{HasPrisoner: true, Liveable: true,
							Prisoner: &prisoners.PrisonerCoop{Prisoner: &prisoners.Prisoner{Uid: maxUID, Age: 0, Points: 0}}, Width: i, Height: k}
					case 2:
						g.fields[i][k] = field.Field{HasPrisoner: true, Liveable: true,
							Prisoner: &prisoners.PrisonerTFT{Prisoner: &prisoners.Prisoner{Uid: maxUID, Age: 0, Points: 0}, Memory: make(map[uint]bool)}, Width: i, Height: k}
					case 3:
						g.fields[i][k] = field.Field{HasPrisoner: true, Liveable: true,
							Prisoner: &prisoners.PrisonerGrudger{Prisoner: &prisoners.Prisoner{Uid: maxUID, Age: 0, Points: 0}, Memory: make(map[uint]bool)}, Width: i, Height: k}
					case 4:
						g.fields[i][k] = field.Field{HasPrisoner: true, Liveable: true,
							Prisoner: &prisoners.PrisonerDetective{Prisoner: &prisoners.Prisoner{Uid: maxUID, Age: 0, Points: 0},
								Memory:             make(map[uint]bool),
								MemoryMeetingCount: make(map[uint]uint),
								MemoryEverCheated:  make(map[uint]bool),
							}, Width: i, Height: k}
					}
					maxUID += 1
				} else {
					g.fields[i][k] = field.Field{HasPrisoner: false, Liveable: true, Prisoner: nil, Width: i, Height: k}
				}
			}
		}
	}

	g.maxUID = maxUID

	ebiten.SetWindowSize(width*pixelSize, height*pixelSize)
	ebiten.SetWindowTitle("Prisoners GO")
	if err := ebiten.RunGame(&g); err != nil {
		log.Fatal(err)
	}
}
