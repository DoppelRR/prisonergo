package util

import "math/rand"

func Mod(a, b int) int {
	m := a % b
	if a < 0 {
		m += b
	}
	return m
}

func IntnZero(max int) int {
	if max == 0 {
		return 0
	} else {
		return rand.Intn(max)
	}
}

func Clamp(val, min, max float64) float64 {
	if val < min {
		return min
	} else if val > max {
		return max
	} else {
		return val
	}
}
