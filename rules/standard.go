package rules

import (
	"fmt"
	"math"
	"math/rand"
	"prisonergo/field"
	"prisonergo/util"
	"strings"
)

const ()

type StandardRules struct {
	Width  int
	Height int

	BreedScore int

	PayoutTemptation int
	PayoutReward     int
	PayoutPunishment int
	PayoutSucker     int
}

func (r *StandardRules) Work(fields [][]field.Field) {
	for w := range fields {
		for h := range fields[w] {
			f := &fields[w][h]
			if !f.HasPrisoner {
				continue
			}
			p := f.Prisoner

			p.IncrementAge()

			for i := -1; i < 2; i++ {
				for k := -1; k < 2; k++ {
					neighBourField := fields[util.Mod(w+i, r.Width)][util.Mod(h+k, r.Height)]
					if !neighBourField.HasPrisoner {
						continue
					}
					p2 := neighBourField.Prisoner
					p1Coops := p.Coop(p2.GetUID())
					p2Coops := p2.Coop(p.GetUID())
					if p1Coops && p2Coops {
						p.AddPoints(r.PayoutReward)
						p2.AddPoints(r.PayoutReward)
					} else if p1Coops && !p2Coops {
						p.AddPoints(r.PayoutSucker)
						p2.AddPoints(r.PayoutTemptation)
					} else if !p1Coops && p2Coops {
						p.AddPoints(r.PayoutTemptation)
						p2.AddPoints(r.PayoutSucker)
					} else if p1Coops && !p2Coops {
						p.AddPoints(r.PayoutPunishment)
						p2.AddPoints(r.PayoutPunishment)
					}
					p.Result(p2.GetUID(), p2Coops)
					p2.Result(p.GetUID(), p1Coops)
				}
			}

			p.AddPoints(int(-1*math.Sqrt(float64(p.GetAge()))) * 3)
		}
	}
}

func (r *StandardRules) Breed(fields [][]field.Field, uid uint) {
	for w := range fields {
		for h := range fields[w] {
			f := &fields[w][h]
			if !f.HasPrisoner {
				continue
			}
			p := f.Prisoner
			if p.GetPoints() > r.BreedScore {
				emptyW, emptyH := r.FindEmptyField(fields, w, h)
				emptyF := &fields[emptyW][emptyH]

				if emptyF.HasPrisoner || !emptyF.Liveable {
					continue
				}

				emptyF.Prisoner = p.Breed(uid)
				emptyF.HasPrisoner = true
			}
		}
	}
}

func (r *StandardRules) Die(fields [][]field.Field) {
	for w := range fields {
		for h := range fields[w] {
			f := &fields[w][h]
			if !f.HasPrisoner {
				continue
			}
			p := f.Prisoner
			if p.GetPoints() < 0 {
				f.HasPrisoner = false
			}
		}
	}
}

func (r *StandardRules) Census(fields [][]field.Field) map[string]uint {
	censusMap := make(map[string]uint)
	var totalCount uint = 0
	for _, row := range fields {
		for _, f := range row {
			if !f.HasPrisoner {
				continue
			}

			p := f.Prisoner

			pType := fmt.Sprintf("%T", p)
			pType = strings.Replace(pType, "*prisoners.Prisoner", "", 1)
			censusMap[pType] += 1
			censusMap["Average Points"] += uint(p.GetPoints())
			censusMap["Average Age"] += uint(p.GetAge())
			totalCount += 1
		}
	}
	if totalCount > 0 {
		censusMap["Average Points"] = censusMap["Average Points"] / totalCount
		censusMap["Average Age"] = censusMap["Average Age"] / totalCount
	}
	return censusMap
}

func (r *StandardRules) FindEmptyField(fields [][]field.Field, w int, h int) (int, int) {
	newW := rand.Intn(3) - 1
	newH := rand.Intn(3) - 1
	return util.Mod(w+newW, r.Width), util.Mod(h+newH, r.Height)
}
