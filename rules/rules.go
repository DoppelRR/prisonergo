package rules

import "prisonergo/field"

type Rules interface {
	Work([][]field.Field)
	Breed([][]field.Field, uint)
	Die([][]field.Field)
	Census([][]field.Field) map[string]uint
	FindEmptyField([][]field.Field, int, int) (int, int)
}
